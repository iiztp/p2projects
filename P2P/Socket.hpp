#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <sys/time.h>
#include <fcntl.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#include <string>
#include <vector>

using namespace std;

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;

class Socket
{
public:
	Socket();
    Socket(SOCKET socket, SOCKADDR_IN sockin);
    Socket(string adress, int port);
    Socket(int port);

    SOCKET getSocket();
    SOCKADDR_IN getSockAddr();

    string getAdress();
    
   	void closeSock();
protected:
	SOCKET sock;
    SOCKADDR_IN sin;
};

#endif