#include "Packet.hpp"

Packet::Packet()
{
	this->type = PACKET_ERROR;
}

Packet::Packet(TYPE type)
{
	this->type = type;
}

Packet::Packet(TYPE type, string content)
{
	this->type = type;
	this->content = content;
}

Packet Packet::getPacketFromString(string buffer)
{
	int first = buffer.find('{');
	int last = buffer.rfind('}');
	string typeS = buffer.substr(0, first);
	string contentS = buffer.substr(first+1, last-(first+1));

	TYPE type;

	if(typeS == "ip")
	{
		type = IP;
	}
	else if(typeS == "message")
	{
		type = MESSAGE;
	}
	else
	{
		return Packet(NO_PACKET);
	}

	return Packet(type, contentS);
}

string Packet::packetToString(TYPE type, string content)
{
	string buffer;
	if(type == IP)
	{
		buffer += "ip{";
	}
	else if(type == MESSAGE)
	{
		buffer += "message{";
	}

	buffer += content;
	buffer += "}";

	return buffer;
}

TYPE Packet::getType()
{
	return type;
}

std::string Packet::getContent()
{
	return content;
}