#ifndef PACKET_HPP
#define PACKET_HPP

#include <string>
#include <iostream>
using namespace std;

enum TYPE
{
	PACKET_ERROR = -1,
	NO_PACKET = 0,
	IP = 1,
	MESSAGE = 2
};

class Packet
{
public:
	Packet();
	Packet(TYPE type);
	Packet(TYPE type, string content);
	static Packet getPacketFromString(string buffer);
	static string packetToString(TYPE type, string content);

	TYPE getType();
	string getContent();
private:
	TYPE type;
	string content;
};

#endif