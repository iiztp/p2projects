#include "Socket.hpp"

Socket::Socket()
{
    this->sock = 0;
}

Socket::Socket(SOCKET socket, SOCKADDR_IN sockin)
{
    this->sock = socket;
    this->sin = sockin;
    int opt = true;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,  sizeof(opt));
    fcntl(sock, F_SETFL, O_NONBLOCK);
}

Socket::Socket(std::string adress, int port)
{
    sock = socket(AF_INET, SOCK_STREAM, 0);
    int opt = true;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,  sizeof(opt));
    fcntl(sock, F_SETFL, O_NONBLOCK);
    sin.sin_addr.s_addr = inet_addr(adress.c_str());
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
}

Socket::Socket(int port)
{
    sock = socket(AF_INET, SOCK_STREAM, 0);
    int opt = true;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,  sizeof(opt));
    fcntl(sock, F_SETFL, O_NONBLOCK);
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
}

string Socket::getAdress()
{
    char str[INET_ADDRSTRLEN];
    return string(inet_ntop(AF_INET, &(sin.sin_addr), str, sizeof(str)));
}

SOCKET Socket::getSocket()
{
    return this->sock;
}

SOCKADDR_IN Socket::getSockAddr()
{
    return this->sin;
}

void Socket::closeSock()
{
    close(sock);
    sock = 0;
}