#include "SocketClient.hpp"

SocketClient::SocketClient() : Socket()
{}
SocketClient::SocketClient(std::string adress, int port) : Socket(adress, port)
{}

bool SocketClient::connectSock()
{
    int timeout = 5000;
    int error;
    while((error = connect(sock, (SOCKADDR*)&sin, sizeof(sin)) != 0) && timeout-- >= 0)
    {
        usleep(1000);
    }

	return (error == 0);
}

Packet SocketClient::receive()
{
    char buffer[32] = "";
    
    int error = recv(getSocket(), &buffer, sizeof(buffer), 0);
    if(error == EWOULDBLOCK || error == 0)
    {
    	return Packet();
    }
    if(error != SOCKET_ERROR)
    {
        return Packet::getPacketFromString(string(buffer));
    }
    
    return Packet(NO_PACKET);
}