#ifndef SOCKET_SERVER
#define SOCKET_SERVER

#include "Socket.hpp"

class SocketServer : public Socket
{
public:
	SocketServer(int port);
	Socket getClient(int index);
	int getNbClients();

	bool bindSock();
	bool listenSock();

	bool isConnection();
	bool isDisconnection();

	Socket getClientConnection();
	Socket getLastConnectedClient();
	Socket getLastDisconnectedClient();

	bool sendPacket(Socket socket, std::string buffer);
private:
	SocketServer(SOCKET sock, SOCKADDR_IN sin);
	std::vector<Socket> clients;
	Socket lastConnectedClient;
	Socket lastDisconnectedClient;
};

#endif