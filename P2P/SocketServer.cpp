#include "SocketServer.hpp"

SocketServer::SocketServer(int port) : Socket(port)
{
}

SocketServer::SocketServer(SOCKET sock, SOCKADDR_IN sin)
{
    this->sock = sock;
    this->sin = sin;
}

bool SocketServer::bindSock()
{
    socklen_t recsize = sizeof(sin);
    return bind(sock, (SOCKADDR*)&sin, recsize) != SOCKET_ERROR;
}

bool SocketServer::listenSock()
{
	return listen(sock, 5) != SOCKET_ERROR;
}

Socket SocketServer::getClient(int index)
{
	return clients[index];
}

int SocketServer::getNbClients()
{
	return clients.size();
}

bool SocketServer::isConnection()
{
    Socket newSocket = getClientConnection();

    if(newSocket.getSocket() == 0)
        return false;
    lastConnectedClient = newSocket;

    return true;
}

bool SocketServer::sendPacket(Socket socket, std::string buffer)
{
    return send(socket.getSocket(), buffer.c_str(), buffer.size(), 0) != SOCKET_ERROR;
}

bool SocketServer::isDisconnection()
{
    for (int i = 0 ; i < clients.size(); i++)   
    {
        Socket current = clients[i];
        char buffer[200];
        int error = recv(current.getSocket(), buffer, 200, 0);
        if(error == EWOULDBLOCK || error == 0)   
        {
            lastDisconnectedClient = clients[i];
            clients.erase(clients.begin() + i);
            current.closeSock();   
            return true;
        }   
    }
    return false;
}

Socket SocketServer::getLastDisconnectedClient()
{
	return lastDisconnectedClient;
}

Socket SocketServer::getLastConnectedClient()
{
    return lastConnectedClient;
}

Socket SocketServer::getClientConnection()
{
	SOCKET csock;
	SOCKADDR_IN csin;
	socklen_t crecsize = sizeof(csin);
	csock = accept(sock, (SOCKADDR*)&csin, &crecsize);

    if(csock == SOCKET_ERROR || csock == EWOULDBLOCK || csock == 0)
        return Socket();

	Socket newSocket(csock, csin);

	clients.push_back(newSocket);

	return newSocket;
}