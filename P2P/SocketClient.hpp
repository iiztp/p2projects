#ifndef SOCKET_CLIENT
#define SOCKET_CLIENT

#include "Socket.hpp"
#include "Packet.hpp"
#include <cerrno>
#include <cstring>

class SocketClient : public Socket
{
public:
	SocketClient();
	SocketClient(string adress, int port);

	bool connectSock();
	Packet receive();
};

#endif