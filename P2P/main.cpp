#include "SocketClient.hpp"
#include "SocketServer.hpp"
#include <thread>
#include <iostream>

#define PORT 44400
#define ADRESS "91.167.189.129"
#define SIZE_RECEIVER 32
//83.194.127.71
//91.167.189.129

using namespace std;

bool connectSock = true;
string message = "";

void server()
{
    SocketServer server(PORT);

    server.bindSock();
    server.listenSock();

    while(connectSock)   
    {
        if(server.isConnection())
        {
            Socket newSocket = server.getLastConnectedClient();
            cout << "Server : Client " << newSocket.getAdress() << " connected on port " << ntohs(newSocket.getSockAddr().sin_port) << endl;
            for(int i = 0; i < server.getNbClients(); i++)
            {
                Socket current = server.getClient(i);
                if(current.getSocket() == newSocket.getSocket())
                    continue;
                server.sendPacket(newSocket, Packet::packetToString(IP, current.getAdress()));
                server.sendPacket(current, Packet::packetToString(IP, newSocket.getAdress()));
            }
        }

        if(message != "")
        {
            for(int i = 0; i < server.getNbClients(); i++)
            {
                Socket current = server.getClient(i);
                server.sendPacket(current, Packet::packetToString(MESSAGE, message));
            }
            message = "";
        }

        if(server.isDisconnection())
        {
            Socket newSocket = server.getLastDisconnectedClient();
            cout << "Server : Client " << newSocket.getAdress() << " disconnected on port " << ntohs(newSocket.getSockAddr().sin_port) << endl;
        }
    }

    for(int i = 0; server.getNbClients(); i++)
    {
        server.getClient(i).closeSock();
    }

    server.closeSock();
}

void client()
{
	vector<SocketClient> clients;

    clients.push_back(SocketClient(ADRESS, PORT));
    if(!clients[0].connectSock())
    {
        cout << "Timeout initial connection, please try again later..." << endl;
        exit(0);
    }
	cout << "Client initial connection to " << clients[0].getAdress() << " on " << htons(clients[0].getSockAddr().sin_port) << endl;
	
    while(connectSock)
    {
        for(int i = 0; i < clients.size(); i++)
        {
            Packet received = clients[i].receive();
            if(received.getType() != PACKET_ERROR)
            {
                if(received.getType() == IP)
                {
                    SocketClient newSocket(received.getContent(), PORT);
                    bool isNewIp = true;

                    for(int i = 0; i < clients.size(); i++)
                    {
                        if(clients[i].getAdress() == newSocket.getAdress())
                            isNewIp = false;

                    }

                    if(isNewIp)
                    {
                        clients.push_back(newSocket);
                        string status;
                        if(newSocket.connectSock())
                        {
                            status = "connected";
                        }
                        else
                        {
                            status = "not connected";
                        }

                        cout << "Client : New host found " << received.getContent() << " on " << htons(newSocket.getSockAddr().sin_port) << ", status : " << status << endl;
                    }
                }

                if(received.getType() == MESSAGE)
                {
                    cout << received.getContent() << endl;
                }

                if(received.getType() == NO_PACKET)
                {

                }
            }
            else
            {
                cout << "Client : Host disconnected " << clients[i].getAdress() << endl;
                clients[i].closeSock();
                clients.erase(clients.begin()+i);
            }
        }
	}

    for(int i = 0; i < clients.size(); i++)
    {
        clients[i].closeSock();
    }
}

void sendMessage()
{
    while(connectSock)
    {
        cout << " > ";
        cin >> message;
    }
}

int main()
{
	std::thread senders(server);
	std::thread senderc(client);
    std::thread messager(sendMessage);

	senders.join();
	senderc.join();
    messager.join();
    
    return EXIT_SUCCESS;
}
